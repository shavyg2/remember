package shavy.remember.storage;

import java.io.Serializable;

public class Cache implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String key;
	String value;
	
	
	
	
	public String key(){
		return key;
	}
	
	
	public String value(){
		return value;
	}
	
	
	public void set(String key,String value){
		this.key=key;
		this.value=value;
		
	}
	
	
	public String get(String key){
		return value;
	}

}
