package shavy.remember;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import shavy.remember.storage.Cache;

public class Remember {

	ArrayList<Cache> Storage;
	String message;

	public Remember() {
		Storage = new ArrayList<Cache>();
		File f = new File(this.dir());
		if (!f.exists()) {
			f.mkdirs();
		}else{
			load();
		}
	}

	public static void main(String[] args) {

		Remember mem = new Remember();
		int count = args.length;
		if (count > 0 && args[0].equals("-list") && count <= 2) {
			if(args.length==2)
			mem.list(args[1]);
			else
				mem.list("");

		} else if (count > 0 && args[0].equals("-set") && count == 3) {

			mem.set(args[1], args[2]);
			System.out.println("Command saved");

		} else if (count > 0 && args[0].equals("-get") && count == 2) {

			Cache c = mem.get(args[1]);
			if (c != null) {
				System.out.println(c.value());
			} else {
				System.out
						.println("Command Not Found\nUse list to see what command you have stored");
			}
		}
		if (mem.save()) {
			// TODO might do something here
			// let them know that everything executed
		} else {
			System.out.println("Error! Command not executed");
		}
	}
	
	public void load(){
		try {
			FileInputStream is= new FileInputStream(this.file());
			ObjectInputStream oi= new ObjectInputStream(is);
			Storage= (ArrayList<Cache>)oi.readObject();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void list(String key) {
		ArrayList<Cache> results= this.resolveList(key);
		
		for(int i=0,count=results.size();i<count;i++){
			System.out.println(results.get(i).key());
		}
	}
	
	public ArrayList<Cache> resolveList(String key){
		ArrayList<Cache> results=new ArrayList<Cache>();
		
		if(key==""){
			
			for(Cache c:Storage){
				if(!c.key().contains(".")){
					results.add(c);
				}
			}
			
		}else{
			for(Cache c:Storage){
				if(CompareList(dotToArray(key), dotToArray(c.key()))){
					
				}
			}
		}
		
		
		return results;
	}
	
	public String[] dotToArray(String dot){
		String[] result=dot.split("\\.");
		return result;
	}
	
	public boolean CompareList(String[] str1,String[] str2){
		if(str1.length+1!=str2.length)
			return false;
		
		
		for(int i=0;i<str1.length;i++){
			if(!str1[i].equals(str2[i])){
				return false;
			}
		}
		
		return true;
	}

	public void set(String key, String value) {
		Cache c = get(key);
		if (c == null) {
			c = new Cache();
			c.set(key, value);
			Storage.add(c);
		}else{
			c.set(key, value);
		}
		
		
	}

	public Cache get(String key) {
		for (Cache c : Storage) {
			if (c.key().equals(key)) {
				return c;
			}
		}

		return null;
	}

	public String dir() {
		return home() + "/.remember";
	}

	public String file() {
		return dir() + "/bank.mem";
	}

	public String home() {
		return System.getenv("AppData");
	}

	public boolean save() {
		return write();
	}

	public boolean write() {
		boolean success = true;
		FileOutputStream fos;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream(file());
			oos = new ObjectOutputStream(fos);
			oos.writeObject(this.Storage);
		} catch (FileNotFoundException e) {
			success = false;
		} catch (IOException e) {
			success = false;
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					success = false;
				}
			}
		}
		return success;

	}

	public String os() {
		return System.getenv("OS");
	}

}
